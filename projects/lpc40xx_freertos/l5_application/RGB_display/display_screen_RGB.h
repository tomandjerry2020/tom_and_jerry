

extern void start_screen_display(void);

extern void pause_screen_display(void);

extern void clear_screen_display(void);

extern void level_display(void);

extern void player_ready_display(void);

extern void player_won_display(void);

extern void game_over_display(void);
